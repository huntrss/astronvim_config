return {
  options = {
    g = {
      icons_enabled = false,
    },
  },
  polish = function()
    vim.opt.wrap = true
  end
}
